import csv
import os
import tempfile
import unittest

from keboola.csvwriter import ElasticDictWriter


class TestCsvTools(unittest.TestCase):

    def test_returns_maintains_initial_header(self):
        fd, fname = tempfile.mkstemp(text=True)

        header = ['a', 'b', 'c', 'd']
        with ElasticDictWriter(fname, header) as wr:
            wr.writeheader()
            wr.writerow({"a": 1})
            wr.writerow({"b": 2, "c": 3})

        rows = []
        expected_rows = [header, ['1', '', '', ''], ['', '2', '3', '']]
        with open(fname, 'r') as file:
            for r in csv.reader(file):
                rows.append(r)

        # cleanup
        os.close(fd)
        os.unlink(fname)

        print(rows)
        self.assertEqual(expected_rows, rows)

    def test_returns_maintains_initial_header_no_header(self):
        fd, fname = tempfile.mkstemp(text=True)

        header = ['a', 'b', 'c', 'd']
        with ElasticDictWriter(fname, header) as wr:
            wr.writerow({"a": 1})
            wr.writerow({"b": 2, "c": 3})

        rows = []
        expected_rows = [['1', '', '', ''], ['', '2', '3', '']]
        with open(fname, 'r') as file:
            for r in csv.reader(file):
                rows.append(r)

        # cleanup
        os.close(fd)
        os.unlink(fname)

        print(rows)
        self.assertEqual(expected_rows, rows)

    def test_same_structure(self):
        fd, fname = tempfile.mkstemp(text=True)

        header = ['a', 'b']
        with ElasticDictWriter(fname, header) as wr:
            wr.writeheader()
            wr.writerow({"a": 1, "b": 2})
            wr.writerow({"a": 1, "b": 2})

        rows = []
        expected_rows = [header, ['1', '2'], ['1', '2']]
        with open(fname, 'r') as file:
            for r in csv.reader(file):
                rows.append(r)

        # cleanup
        os.close(fd)
        os.unlink(fname)

        print(rows)
        self.assertEqual(expected_rows, rows)

    def test_returns_expands_initial_header(self):
        fd, fname = tempfile.mkstemp(text=True)

        header = ['a', 'b', 'c']
        with ElasticDictWriter(fname, header) as wr:
            wr.writeheader()
            wr.writerow({"a": 1})
            wr.writerow({"b": 2, "c": 3, "d": 4})

        rows = []
        expected_rows = [['a', 'b', 'c', 'd'], ['', '2', '3', '4'], ['1', '', '', '']]
        with open(fname, 'r') as file:
            for r in csv.reader(file):
                rows.append(r)

        # cleanup
        os.close(fd)
        os.unlink(fname)

        print(rows)
        self.assertEqual(expected_rows, rows)

    def test_returns_expands_initial_header_no_with(self):
        fd, fname = tempfile.mkstemp(text=True)

        header = ['a', 'b', 'c']
        wr = ElasticDictWriter(fname, header)
        wr.writeheader()
        wr.writerow({"a": 1})
        wr.writerow({"b": 2, "c": 3, "d": 4})
        wr.close()
        rows = []
        expected_rows = [['a', 'b', 'c', 'd'], ['', '2', '3', '4'], ['1', '', '', '']]
        with open(fname, 'r') as file:
            for r in csv.reader(file):
                rows.append(r)

        # cleanup
        os.close(fd)
        os.unlink(fname)

        print(rows)
        self.assertEqual(expected_rows, rows)
